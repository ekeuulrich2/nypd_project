package com.tp.spark.core

import org.joda.time.{DateTime, DateTimeZone}
import play.api.libs.json.{JsValue, Json}

import scala.util.Random

class Drones(val id: Int) {


  val cities: List[String] = List("NY", "NJ", "CT","PA","NE","MD","MA","99","RI","OL","HF","VF","NP","BF","AS","CH","JK","LS","MP","UK","VP")
  val typeIdentifiable: List[Boolean] = List(true, false)
  val plateIds: List[String] = List("GBB9093", "62416MB", "78755JZ","63009MA","91648MC","T60DAR","GCR2838","XZ764G","GBH9379","MCL78B","M367CN","GAR6813","GEN8674","GAC2703","40793JY","GAD1485","GFC5338","815M342","GJA3452","YZY6476","WBJ819")
  val violationCodes: List[Int] = List(46,14,75,86,95,87,55,78,12,45,43,66,99,23,24,19,53,34,82,26,84,25,66,85,75,91)
  val summonsNumbers: List[String] = List("1283294138", "1283294138", "1283294163","1283294175","1283294187","1283294217","1283294229","1283983620","1286248000","1286282330","1286282342","1286246416","1286246398","1286123550","1286036800","1283983825","1283983771","1283983734","1283983679","1283983667","1283983631")



  var IDdrone: Int = id

  //Pourcentage batterie
  var niveauBatterie: Int = 100

  //Altitude en mètre
  var droneAltitude: Int = 0

  //Température en dégré Celcius
  var temperature: Int = 0

  //Vitesse en km/h
  var vitesse : Int = 0

  //Espace disque disponible en MB
  var espace_disque: Int = 5000

  //Temps
  var temps: Long = DateTime.now(DateTimeZone.UTC).getMillis

  //City
  var ville = getRandomElements(cities)

  //Identifiable
  var identifiable = getRandomElements(typeIdentifiable)

  //Plaque
  var plaque = getRandomElements(plateIds)

  //Violation
  var violationCode = getRandomElements(violationCodes)

  //Summons Number
  var summonsNumber = getRandomElements(summonsNumbers)

  //Paramètre par défaut Lati et Long
  var latitude: Double = 57.3648
  var longitude: Double = 8.5364

  def infoDrone(): Unit = {
    println ("Drone id: " + IDdrone);
    println ("Position Latitude : " + latitude);
    println ("Position Longitude : " + longitude);
    println ("Ville D'étude : " + ville);
  }

  def messageJSON(): String = {

    val messageDroneJSON: JsValue = Json.parse(
      s"""
         |{
         |  "idDrone" : ${IDdrone},
         |  "niveauBatterie" : ${niveauBatterie},
         |  "droneAltitude" : ${droneAltitude},
         |  "vitesse" : ${vitesse},
         |  "espaceDisque" : ${espace_disque},
         |  "villeEtude" : "${ville}",
         |  "temperature" : ${temperature},
         |  "time" : ${temps},
         |  "identifiable" : ${identifiable},
         |  "plaqueIdentifier" : "${plaque}",
         |  "violationCode" : ${violationCode},
         |  "summonsNumber" : "${summonsNumber}",
         |  "emplacement" : {
         |    "latitude" : ${latitude},
         |    "longitude" : ${longitude}
         |  }
         |}
         |""".stripMargin)

      Json.stringify(messageDroneJSON)
  }


  def simulDrone(): Unit ={
    val random = new Random()
    temperature = 37 + 1
    niveauBatterie = niveauBatterie - 2
    droneAltitude = droneAltitude + 1 + random.nextInt(4)
    vitesse = 20 + random.nextInt(60)
    espace_disque = espace_disque - random.nextInt(30 + random.nextInt(20))
    latitude = latitude + random.nextDouble()
    longitude = longitude + random.nextDouble()
    summonsNumber = getRandomElements(summonsNumbers)
    identifiable = getRandomElements(typeIdentifiable)
    violationCode = getRandomElements(violationCodes)
    plaque = getRandomElements(plateIds)
    ville = getRandomElements(cities)

  }
  def getRandomElements(list: List[Any]): Any = {
    Random.shuffle(list).head
  }


}
