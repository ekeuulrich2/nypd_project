package com.tp.spark.core

import java.util.{Properties, TimerTask}

import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}

object NYPDStreamingDroneProducer extends App{

  val props = new Properties()
  props.put("bootstrap.servers", "localhost:9092")
  props.put("key.serializer","org.apache.kafka.common.serialization.StringSerializer")
  props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
  val droneProducer = new KafkaProducer[String, String](props)
  val droneTopic = "infosDrone"

  val drone1 = new Drones(1)
  val drone2 = new Drones(2)
  val drone3 = new Drones(3)
  val drone4 = new Drones(4)
  val drone5 = new Drones(5)

  val nypdDrones: List[Drones] = List(drone1,drone2,drone3,drone4,drone5)

  envoisDesDonneesDronesVersKafka(nypdDrones, 5000)

  def envoisDesDonneesDronesVersKafka(nypdDrones: List[Drones], tempExec: Int): Unit = {
    val temps = new java.util.Timer()
    val task: TimerTask = new java.util.TimerTask {
      println("Hello")
      def run(): Unit = {
        println("Envoi des données des 5 drones")
        nypdDrones.foreach { nypdDrone =>
           println(nypdDrone)
           val donnee = new ProducerRecord(droneTopic, nypdDrone.IDdrone.toString, nypdDrone.messageJSON())
           droneProducer.send(donnee)
           nypdDrone.simulDrone()
        }
      }
    }
    temps.schedule(task, tempExec, tempExec)
  }


}
