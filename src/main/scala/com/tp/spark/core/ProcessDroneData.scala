package com.tp.spark.core

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.rdd.RDD

object ProcessDroneData extends App {

  def chargementDonnee(): RDD[DroneUtilities.Drone] = {

    val conf = new SparkConf()
      .setAppName("donneeDrone")
      .setMaster("local[*]")
    val ssc = SparkContext.getOrCreate(conf)

    ssc.textFile("D:\\Udemy_Courses\\NYPD_PROJECT\\Drone_0.JSON")
      .mapPartitions(DroneUtilities.convertFromJson)
  }

  def checkTemparature(): Unit ={
    val read = chargementDonnee()
      .map(dst => ((dst.temperature.toInt, dst.IDdrone), (dst.droneAltitude, dst.emplacement.latitude, dst.emplacement.longitude, dst.vitesse, 1)))
      .reduceByKey((dstData, index) =>
        (index._1 + dstData._1,
        dstData._2 + index._2,
        dstData._3 + index._3,
        dstData._4 + index._4,
        dstData._5 + index._5)
      )
      .map(dataTuple => (dataTuple._1._1,
      dataTuple._2._1 / dataTuple._2._5
        , dataTuple._2._2 / dataTuple._2._5
        , dataTuple._2._3 / dataTuple._2._5
        , dataTuple._2._4 / dataTuple._2._5
        , dataTuple._2._5 / dataTuple._2._5
      , dataTuple._1._2)
      )
      .sortBy(_._1)

    read.foreach(ex => println(s"""Le drone ${ex._7} [ Température => { ${ex._1} degré celcius } , Emplacement => { Latitude: ${ex._3}, Longitude: ${ex._4}} , Altitude => { ${ex._2} ft }, Vitesse => { ${ex._5} m/s } ]"""))
  }

  def checkBatterie(): Unit = {
    val read = chargementDonnee()
      .map(dst => ((dst.niveauBatterie.toInt, dst.IDdrone), (dst.vitesse, 1)))
      .reduceByKey((dstData, index) => (index._1 + dstData._1, dstData._2 + index._2))
      .map(dataTuple => (dataTuple._1._1,
        dataTuple._2._1 / dataTuple._2._2,
        dataTuple._2._2,
        dataTuple._1._2
      ))
      .sortBy(_._1)

    read.foreach(ex => println(s"""Le drone ${ex._3} [ Niveau de batterie => { ${ex._1} % }, Vistesse en moyenne => { ${ex._2} km/h ${ex._3 * 35} secondes } ]"""))
  }

  def droneDefaillant(): Unit = {
    val read = chargementDonnee()
      .foreach(dst => {
        if (dst.temperature > 60) println(s"""La température du drone ${dst.IDdrone} est très elévé""")
      })
  }

  def vehiculePasIdentifiable(): Unit = {
    val read = chargementDonnee()
      .foreach(dst => {
        if (dst.identifiable == false) println(s"""La drone ${dst.IDdrone} n'arrive pas a identifier le vehicule avec pour plaque ${dst.plaque} dans la ville de ${dst.ville}""")
      })
  }

  def contientDonnee(read: Array[(Int, Float)], index: Int): Boolean = {
    var iVal = 0
    for ( iVal <- index+1 to read.length) {
      if (read(iVal)._1 == read(index)._1) return true
    }
    false
  }

  def droneEnPanne(): Unit = {
    val read = chargementDonnee()
      .sortBy(_.temp)
      .map(dst => (dst.IDdrone, dst.droneAltitude))
      .collect()

    var index = 0
    for (index <- 0 to read.length -2) {
      if (read(index + 1)._2 - read(index)._2 > 60 && contientDonnee(read, index) == false) {
        println(read(index)._1 + " a des soucis....!")
      }
    }
  }

  def startAnalyzing(): Unit = {
    checkTemparature()
    checkBatterie()
    droneDefaillant()
    droneEnPanne()
    vehiculePasIdentifiable()
  }
}
