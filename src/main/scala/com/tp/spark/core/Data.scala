package com.tp.spark.core

import java.util.Date

case class Data (SummonsNumber:String,
                 PlateID: String,
                 RegistrationState: String,
                 PlateType: String,
                 IssueDate: String,
                 ViolationCode: String,
                 VehicleBodyType:String,
                 VehicleMake: String,
                 IssuingAgency: String,
                 StreetCode1: String,
                 StreetCode2: String,
                 StreetCode3: String,
                 VehicleExpirationDate:String,
                 ViolationLocation: String,
                 ViolationPrecinct:String,
                 IssuerPrecinct:String,
                 IssuerCode:String,
                 IssuerCommand: String,
                 IssuerSquad:String,
                 ViolationTime: String) {
}
