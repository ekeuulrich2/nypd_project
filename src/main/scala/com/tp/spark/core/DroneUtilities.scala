package com.tp.spark.core

import com.google.gson.Gson


object DroneUtilities {

  case class Emplacement (
                         latitude: Float,
                         longitude: Float
                         )
  case class Drone (
                  IDdrone: Int,
                  temperature: Int,
                  temp: Long,
                  niveauBatterie: Int,
                  droneAltitude: Float,
                  vitesse: Float,
                  espace_disque: Int,
                  identifiable: Boolean,
                  summonsNumber: String,
                  violationCode: Int,
                  plaque: String,
                  ville: String,
                  emplacement: Emplacement
                  )
  def convertFromJson(lines: Iterator[String]): Iterator[Drone] = {
    val typeGson = new Gson()
    lines.map(line => {
      typeGson.fromJson(line, classOf[Drone])
    })
  }

}
