package com.tp.spark.core


import java.util.Properties

import org.apache.kafka.common.TopicPartition
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.streaming.kafka010._
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.SparkConf
import com.datastax.spark.connector._
import com.datastax.spark.connector.cql.CassandraConnector
import org.apache.kafka.clients.consumer.{ConsumerConfig, KafkaConsumer}
import play.api.libs.json.Json



object NYPDStreamingConsumer {

  def main(args: Array[String]): Unit = {


    val conf = new SparkConf()
    conf.set("spark.connector.connection.host", "127.0.0.1")
    conf.setMaster("local[*]")
    conf.setAppName("NYPDStreamingConsumer")

    val ssc = new StreamingContext(conf, Seconds(30))

    val topics = List("processing")
    val preferredHosts = LocationStrategies.PreferConsistent
    val kafkaParams = Map(
      "bootstrap.servers" -> "localhost:9092",
      "key.deserializer" -> classOf[StringDeserializer],
      "value.deserializer" -> classOf[StringDeserializer],
      "group.id" -> "spark-streaming-notes",
      "auto.offset.reset" -> "earliest"
    )

    val offsets = Map(new TopicPartition("processing", 0) -> 2L)

    val dstream = KafkaUtils.createDirectStream[String, String](
      ssc,
      preferredHosts,
      ConsumerStrategies.Subscribe[String, String](topics, kafkaParams, offsets))


    dstream.foreachRDD((cr, time)  => {
      val message = cr.map(_.value())
      message.foreach(str => {
        val JsonMessage = Json.parse(str)
        CassandraConnector(conf).withSessionDo{ session =>
          session.execute(s"""INSERT INTO scalaproject.nypd JSON '${JsonMessage}'""")
        }
      })

    })

    ssc.start()
    ssc.awaitTermination()

  }

}
