package com.tp.spark.core

import java.io.FileWriter
import java.util
import java.util.Properties
import scala.collection.JavaConverters._
import com.datastax.spark.connector.util
import org.apache.kafka.clients.consumer.{ConsumerConfig, KafkaConsumer}


object NYPDStreamingDroneConsumer extends App {

  val DRONETOPIC = Seq("infosDrone")

  val  props = new Properties()
  props.put("bootstrap.servers", "localhost:9092")
  props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false")
  props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
  props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
  props.put("group.id", "droneConsumer")

  val consumer = new KafkaConsumer[String, String](props)
  consumer.subscribe(DRONETOPIC.asJava)

recuperationDonneeDrones(5000)


  def recuperationDonneeDrones(tempsD: Int): Unit = {

    val temps = new java.util.Timer()
    val tache = new java.util.TimerTask {
      def run(): Unit = {
        var count: Int = 0
        println("Recuperation en cour des drones...")
        val donnees = consumer.poll(5)
        for (donnee <- donnees.asScala) {
          println(donnee)
          val fileWriter = new FileWriter("Drone_"+count+".JSON", true)
          try{
            fileWriter.write(donnee.value()+"\n")
          }
          finally  fileWriter.close()

          count += 1
          println(donnee)
        }
      }
    }
    temps.schedule(tache, tempsD, tempsD)
    ProcessDroneData.startAnalyzing()
  }

}
