package com.tp.spark.core

import java.util.{Date, Properties}

import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.ProducerRecord
import play.api.libs.json._

import scala.io.Source

object NYPDStreamingProducer {

  def main(args: Array[String]): Unit = {
    sendCSVFiles("processing")

  }

  def sendCSVFiles(topic: String): Unit = {


    val props = new Properties()
    props.put("bootstrap.servers", "localhost:9092")
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")

    val myProducer = new KafkaProducer[String,String](props)

    val bufferedSource = Source.fromFile("Parking_Violations_Issued_-_Fiscal_Year_2017.csv")

    implicit val dataTransfer: Writes[Data] = Json.writes[Data]
    for (line <- bufferedSource.getLines){
      val cols = line.split(",").map(_.trim)
      if (cols(0) != "Summons Number"){
        val data = Data(SummonsNumber = s"${cols(0)}",
          PlateID = s"${cols(1)}",
          RegistrationState = s"${cols(2)}",
          PlateType = s"${cols(3)}",
          IssueDate = s"${cols(5)}",
          ViolationCode = s"${cols(5)}",
          VehicleBodyType = s"${cols(6)}",
          VehicleMake = s"${cols(7)}",
          IssuingAgency = s"${cols(8)}",
          StreetCode1 = s"${cols(9)}",
          StreetCode2 = s"${cols(10)}",
          StreetCode3 = s"${cols(11)}",
          VehicleExpirationDate= s"${cols(12)}",
          ViolationLocation= s"${cols(13)}",
          ViolationPrecinct= s"${cols(14)}",
          IssuerPrecinct= s"${cols(15)}",
          IssuerCode = s"${cols(16)}",
          IssuerCommand = s"${cols(17)}",
          IssuerSquad = s"${cols(18)}",
          ViolationTime = s"${cols(19)}")
        val dataJSON = Json.toJson(data).toString()
        val record = new ProducerRecord[String,String](topic, dataJSON)
        myProducer.send(record)
      }
    }
    myProducer.close()

  }
  

}
