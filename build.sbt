name := "NYPD_PROJECT"

version := "0.1"

scalaVersion := "2.11.8"

dependencyOverrides += "com.fasterxml.jackson.core" % "jackson-core" % "2.8.7"
dependencyOverrides += "com.fasterxml.jackson.core" % "jackson-databind" % "2.8.7"
dependencyOverrides += "com.fasterxml.jackson.module" % "jackson-module-scala_2.11" % "2.8.7"

libraryDependencies += "org.apache.kafka" % "kafka-clients" % "0.11.0.1"
libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.1.3" % Runtime
//libraryDependencies += "net.logstash.logback" % "logstash-logback-encoder" % "2.3"
libraryDependencies += "org.scala-lang.modules" %% "scala-parser-combinators" % "1.1.2"
libraryDependencies += "com.typesafe.play" %% "play-json" % "2.7.0"
libraryDependencies += "org.apache.spark" %% "spark-sql" % "2.4.4"
libraryDependencies += "org.apache.spark" % "spark-streaming_2.11" % "2.4.4"
libraryDependencies += "org.apache.spark" % "spark-streaming-kafka-0-10_2.11" % "2.4.4"
libraryDependencies += "com.datastax.spark" % "spark-cassandra-connector_2.11" % "2.4.1"
libraryDependencies += "com.datastax.cassandra" % "cassandra-driver-core" % "3.0.0"
libraryDependencies += "io.netty" % "netty-all" % "4.1.34.Final"
//libraryDependencies += "io.netty" % "netty-buffer" % "4.1.7.Final"
libraryDependencies += "org.typelevel" %% "cats-core" % "1.6.0"
libraryDependencies += "com.google.guava" % "guava" % "14.0"
libraryDependencies += "com.google.code.gson" % "gson" % "2.2.4"

